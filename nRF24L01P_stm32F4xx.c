#ifdef STM32F4XX

#include "nRF24L01P_stm32F4xx.h"

//***************************************************//
//Define the layer1 functions

void nRF24L01_HW_SPI_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef  SPI_InitStructure;

  /* Enable SPI GPIO clocks */
  RCC_AHB1PeriphClockCmd(RCC_AHBPeriph_GPIO_SPI, ENABLE);
  
  if ((SPI == SPI2)||(SPI == SPI3))
  {
    /* Enable SPI Periph clock */
    RCC_APB1PeriphClockCmd(RCC_APBPeriph_SPI, ENABLE);
  }
  else
  {
    /* Enable SPI Periph clock */
    RCC_APB2PeriphClockCmd(RCC_APBPeriph_SPI, ENABLE);
  }

  /* Enable GPIO of CHIP SELECT */
  RCC_AHB1PeriphClockCmd(RCC_AHBPeriph_GPIO_CS_CE, ENABLE);
	
  /* Connect SPI pins to AF */
  //GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_CS_SOURCE,GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_SCK_SOURCE,GPIO_AF_SPI);
  GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_MOSI_SOURCE,GPIO_AF_SPI);
  GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_MISO_SOURCE,GPIO_AF_SPI);
	
  /* Configure SPI pins:  SCK ,MOSI, MISO */
  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_SPI_SCK | GPIO_Pin_SPI_MOSI | GPIO_Pin_SPI_MISO;
  GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_Init(GPIO_SPI, &GPIO_InitStructure);

  /* Configure CS pin */
  //SPI_SSOutputCmd(SPI, ENABLE);
  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_CS|GPIO_Pin_CE;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_DOWN;
  GPIO_Init(GPIO_CS_CE, &GPIO_InitStructure);

  /* SPI configuration */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; // 84000kHz/16=5250kHz
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI, &SPI_InitStructure);

  SPI_CalculateCRC(SPI1, DISABLE);

  /* SPI1 enable */
  SPI_Cmd(SPI, ENABLE);
}

void nRF24L01_HW_EXTI_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable GPIO of EXTI */
  RCC_AHB1PeriphClockCmd(RCC_AHBPeriph_GPIO_IRQnrf, ENABLE);
  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Configure EXTI pin */
  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_IRQnrf;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_NOPULL;
  GPIO_Init(GPIO_IRQnrf, &GPIO_InitStructure);

  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIO_IRQnrf, EXTI_PinSource_IRQnrf);
  
  EXTI_StructInit(&EXTI_InitStructure);
  EXTI_InitStructure.EXTI_Line = EXTI_Linenrf;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  NVIC_InitStructure.NVIC_IRQChannel = EXTI_IRQnrf;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0A;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void nRF24L01_HW_Init(void)
{
  nRF24L01_HW_SPI_Init();
  nRF24L01_HW_EXTI_Init();
}

void nRF24L01_SPI_NSS_L(void)
{
  GPIO_ResetBits(GPIO_CS_CE, GPIO_Pin_CS);
}

void nRF24L01_SPI_NSS_H(void)
{
  GPIO_SetBits(GPIO_CS_CE, GPIO_Pin_CS);
}

void nRF24L01_CE_L(void)
{
  GPIO_ResetBits(GPIO_CS_CE, GPIO_Pin_CE);
}

void nRF24L01_CE_H(void)
{
  GPIO_SetBits(GPIO_CS_CE, GPIO_Pin_CE);
}

unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat)
{
  /* Loop while DR register in not emplty */
  while(SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_TXE) == RESET);

  /* Send byte through the SPIx peripheral */
  SPI_I2S_SendData(SPI, dat);

  /* Wait to receive a byte */
  while(SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_RXNE) == RESET);

  /* Return the byte read from the SPI bus */
  return SPI_I2S_ReceiveData(SPI);
}

void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i;

  for(i=0; i<Len; i++)
  {
    nRF24L01_SPI_SendRecv_Byte(*pBuf);
    pBuf++;
  }
}

void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i;

  for(i=0;i<Len;i++)
  {
     pBuf[i] = nRF24L01_SPI_SendRecv_Byte(0xFF);
  }
}

void __attribute__((optimize("O0"))) nRF24L01_Delay_us(unsigned long n)
{
  for(n=n*17-4; n>0; n--);
}

#endif