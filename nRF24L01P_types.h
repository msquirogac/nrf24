#ifndef __NRF_TYPES
#define __NRF_TYPES

#include <stdint.h>

typedef struct {

	int8_t adr_width;
	int8_t pload_width[6];

	int8_t feature;
	int8_t en_aa;
	int8_t dynpd;

	int8_t channel;
	int8_t config;
	int8_t rf_setup;
	int8_t en_rxaddr;
  
	int8_t setup_retr;
  
	int8_t status;
	int8_t rpd;
	int8_t fifo_status;

	int8_t tx_addr[5];
	int8_t rx_addr[5];
	int8_t rx_addr1[5];
	int8_t rx_addr2[1];
	int8_t rx_addr3[1];
	int8_t rx_addr4[1];
	int8_t rx_addr5[1];

} nRF_InitTypeDef;

typedef struct {

	uint8_t data[32];
	uint8_t size;
	uint8_t flag;
	uint8_t pipe;

} nRF_RXDataTypeDef;

typedef struct {

	uint8_t data[32];
	uint8_t size;
	uint8_t flag;

} nRF_TXDataTypeDef;

#endif