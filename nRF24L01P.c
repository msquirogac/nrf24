#include "nRF24L01P.h"

nRF_InitTypeDef __nRF24L01;

//***************************************************//
//Define the layer2 functions

unsigned char SPI_SR_Reg(void)
{
  unsigned char reg_val;

  nRF24L01_SPI_NSS_L();                       // CSN low, initialize SPI communication...
  reg_val = nRF24L01_SPI_SendRecv_Byte(NOP);  // Read register value
  nRF24L01_SPI_NSS_H();                       // CSN high, terminate SPI communication

  return(reg_val);                            // return register value
}

unsigned char SPI_RD_Reg(unsigned char reg)
{
  unsigned char reg_val;

  nRF24L01_SPI_NSS_L();                       // CSN low, initialize SPI communication...
  nRF24L01_SPI_SendRecv_Byte(reg);            // Select register to read from..
  reg_val = nRF24L01_SPI_SendRecv_Byte(NOP);  // ..then read register value
  nRF24L01_SPI_NSS_H();                       // CSN high, terminate SPI communication

  return(reg_val);                            // return register value
}

unsigned char SPI_WR_Reg(unsigned char reg, unsigned char value)
{
  unsigned char status;

  nRF24L01_SPI_NSS_L();                       // CSN low, initialize SPI communication...
  status = nRF24L01_SPI_SendRecv_Byte(reg);   // select register
  nRF24L01_SPI_SendRecv_Byte(value);          // ..and write value to it..
  nRF24L01_SPI_NSS_H();                       // CSN high, terminate SPI communication

  return(status);                             // return nRF24L01 status unsigned char
}

unsigned char SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char Len)
{
  unsigned int status;

  nRF24L01_SPI_NSS_L();                  // CSN low, initialize SPI communication...
  status = nRF24L01_SPI_SendRecv_Byte(reg);  // Select register to write to and read status unsigned char
  nRF24L01_SPI_Recv_nByte(pBuf,Len);
  nRF24L01_SPI_NSS_H();                  // CSN high, terminate SPI communication

  return(status);                        // return nRF24L01 status unsigned char
}

unsigned char SPI_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned char Len)
{
  unsigned int status;

  nRF24L01_SPI_NSS_L();                  // CSN low, initialize SPI communication...
  status = nRF24L01_SPI_SendRecv_Byte(reg);
  nRF24L01_SPI_Send_nByte(pBuf,Len);
  nRF24L01_SPI_NSS_H();                  // CSN high, terminate SPI communication

  return(status);   
}

//***************************************************//
//Define the layer3:application operation

void nRF24L01_Init(void)
{
  __nRF24L01.adr_width = DEFAULT_ADR_WIDTH;
  __nRF24L01.pload_width[0] = DEFAULT_PAYLOAD;
  __nRF24L01.channel = DEFAULT_CHANNEL;
  __nRF24L01.config = DEFAULT_CONFIG;
  __nRF24L01.rf_setup = DEFAULT_RF_SETUP;
	__nRF24L01.setup_retr = DEFAULT_SETUP_RETR;
  __nRF24L01.feature = DEFAULT_FEATURE;
  __nRF24L01.en_aa  = DEFAULT_EN_AA;
  __nRF24L01.en_rxaddr = DEFAULT_EN_RXADDR;

  nRF24L01_HW_Init();

  nRF24L01_CE_L();
  nRF24L01_SPI_NSS_H();

  nRF24L01_Delay_us(150000);

  nRF24L01_TxFlush();
  nRF24L01_RxFlush();

  nRF24L01_ClearITPendingBit(nRF_IT_ALL);
}

void nRF24L01_PowerRadio(FunctionalState state)
{
  if(state == ENABLE)
  {
    // Set Power Up
    __nRF24L01.config |= CONFIG_PWR_UP;
  }
  else
  {
    // Set Power Down
    __nRF24L01.config &= ~CONFIG_PWR_UP;
  }

  SPI_WR_Reg(WRITE_nRF_REG + CONFIG, __nRF24L01.config);
  nRF24L01_Delay_us(5000);
}

void nRF24L01_Cmd(FunctionalState state)
{
  if(state == ENABLE)
    nRF24L01_CE_H();
  else
    nRF24L01_CE_L();
}

void nRF24L01_Setup(void)
{
  // Setup AutoAck
  SPI_WR_Reg(WRITE_nRF_REG + EN_AA, __nRF24L01.en_aa);
  nRF24L01_Delay_us(5);

  // Enable rx address
  SPI_WR_Reg(WRITE_nRF_REG + EN_RXADDR, __nRF24L01.en_rxaddr);
  nRF24L01_Delay_us(5);

  // Setup add width 5 bytes
  SPI_WR_Reg(WRITE_nRF_REG + SETUP_AW, 0x03);
  nRF24L01_Delay_us(5);

  // Setup Auto Retransmit Delay, Auto Retransmit Count
  SPI_WR_Reg(WRITE_nRF_REG + SETUP_RETR, __nRF24L01.setup_retr);
  nRF24L01_Delay_us(5);

  // Setup channel
  SPI_WR_Reg(WRITE_nRF_REG + RF_CH,__nRF24L01.channel);
  nRF24L01_Delay_us(5);

  //Setup power and rate
  SPI_WR_Reg(WRITE_nRF_REG + RF_SETUP ,__nRF24L01.rf_setup);
  nRF24L01_Delay_us(5);

  // Payload width setup
  SPI_WR_Reg(WRITE_nRF_REG + RX_PW_P0, __nRF24L01.pload_width[0]);
  nRF24L01_Delay_us(5);

	// Setup Config
  SPI_WR_Reg(WRITE_nRF_REG + CONFIG, __nRF24L01.config);
  nRF24L01_Delay_us(5);
}

void nRF24L01_RX_Mode(void)
{
  nRF24L01_Cmd(DISABLE);

  // Set Config Rx
  __nRF24L01.config |= CONFIG_PRIM_RX;
  SPI_WR_Reg(WRITE_nRF_REG + CONFIG, __nRF24L01.config);
  nRF24L01_Delay_us(135);

  nRF24L01_Cmd(ENABLE);
}

void nRF24L01_TX_Mode(void)
{
  nRF24L01_Cmd(DISABLE);

  __nRF24L01.config &= ~CONFIG_PRIM_RX;
  SPI_WR_Reg(WRITE_nRF_REG + CONFIG, __nRF24L01.config);
  nRF24L01_Delay_us(135);
}

unsigned char nRF24L01_RxPacket(nRF_RXDataTypeDef* RX_Packet, FunctionalState dynamic)
{
  unsigned char flag=0;

  if(!(__nRF24L01.fifo_status & FIFO_STATUS_RX_EMPTY))                    //Data Ready RX FIFO interrupt
  {
    (*RX_Packet).pipe = (__nRF24L01.status & STATUS_RX_P_NO)>>1;
    if(dynamic == ENABLE)
    {
      (*RX_Packet).size = nRF24L01_GetLastPayloadSize();
    }
    else
    {
      (*RX_Packet).size = __nRF24L01.pload_width[(*RX_Packet).pipe];
    }

    if((*RX_Packet).size > 32)
    {
      nRF24L01_RxFlush();
    }
    else
    {
      SPI_Read_Buf(RD_RX_PLOAD,(*RX_Packet).data,(*RX_Packet).size);
      flag =1;
    }
  }
  return flag;
}

void nRF24L01_TxPacket(nRF_TXDataTypeDef* TX_Packet)
{
	nRF24L01_Cmd(DISABLE);

  if(__nRF24L01.status & STATUS_TX_DS)                                        // if receive data ready (TX_DS) interrupt
  {
    nRF24L01_ClearITPendingBit(nRF_IT_TX_DS);
  }

  if(__nRF24L01.status & STATUS_MAX_RT)                                       // if receive data ready (MAX_RT) interrupt, this is retransmit than  SETUP_RETR
  {
    nRF24L01_ClearITPendingBit(nRF_IT_MAX_RT);
    nRF24L01_TxFlush();
  }

  SPI_Write_Buf(WR_TX_PLOAD, (*TX_Packet).data, (*TX_Packet).size);    // Write payload to TX_FIFO
  //nRF24L01_Delay_us(20);

  nRF24L01_Cmd(ENABLE);
  nRF24L01_Delay_us(11);
  nRF24L01_Cmd(DISABLE);
}

unsigned char nRF24L01_RxPacketNb(nRF_RXDataTypeDef* RX_Packet, FunctionalState dynamic)
{
	nRF24L01_Status();
  nRF24L01_FifoStatus();
  return nRF24L01_RxPacket(RX_Packet, dynamic);
}

void nRF24L01_TxPacketNb(nRF_TXDataTypeDef* TX_Packet)
{
  nRF24L01_Status();
  nRF24L01_TxPacket(TX_Packet);
}

void nRF24L01_TxPacketAck(nRF_TXDataTypeDef* TX_Packet, unsigned char pipe)
{
  if((pipe >= 0)&&(pipe <= 5))
  {
    SPI_Write_Buf(W_ACK_PLOAD+pipe, (*TX_Packet).data, (*TX_Packet).size);    // Write payload to TX_FIFO
  }
}

unsigned char nRF24L01_TXSuccess(void)
{ 
  __nRF24L01.status = SPI_SR_Reg();
  if(__nRF24L01.status & STATUS_TX_DS)
    return 1;
  else
    return 0;
}

unsigned char nRF24L01_RXSuccess(void)
{ 
  __nRF24L01.status = SPI_SR_Reg();
  if(__nRF24L01.status & STATUS_RX_DR)
    return 1;
  else
    return 0;
}


void nRF24L01_TxFlush(void)
{
  nRF24L01_SPI_NSS_L();                             // CSN low, initialize SPI communication...
  
  nRF24L01_SPI_SendRecv_Byte(FLUSH_TX);
  nRF24L01_SPI_SendRecv_Byte(0x00);

  nRF24L01_SPI_NSS_H();                             // CSN high, terminate SPI communication
}

void nRF24L01_RxFlush(void)
{
  nRF24L01_SPI_NSS_L();                             // CSN low, initialize SPI communication...

  nRF24L01_SPI_SendRecv_Byte(FLUSH_RX);
  nRF24L01_SPI_SendRecv_Byte(0x00);
  
  nRF24L01_SPI_NSS_H();                             // CSN high, terminate SPI communication
}

int8_t nRF24L01_Status(void)
{
  __nRF24L01.status = SPI_SR_Reg();
  return __nRF24L01.status;
}

int8_t nRF24L01_GetStatus(void)
{
  return __nRF24L01.status;
}

int8_t nRF24L01_FifoStatus(void)
{
  __nRF24L01.fifo_status = SPI_RD_Reg(FIFO_STATUS);
  return __nRF24L01.fifo_status;
}

int8_t nRF24L01_GetFifoStatus(void)
{
  return __nRF24L01.fifo_status;
}

int8_t nRF24L01_ITStatus(void)
{
  __nRF24L01.status = SPI_SR_Reg();
  return (__nRF24L01.status & nRF_IT_MASK);
}

int8_t nRF24L01_GetITStatus(int8_t irq)
{
  return (__nRF24L01.status & nRF_IT_MASK & irq);
}

void nRF24L01_ClearITPendingBit(int8_t irq)
{
  uint8_t tmp = irq & nRF_IT_MASK;
  __nRF24L01.status &= ~(tmp);
  SPI_WR_Reg(WRITE_nRF_REG+NRFRegSTATUS, tmp); // Write 1 to clear bit
}

void nRF24L01_ITConfig(unsigned char interrupt, FunctionalState state)
{
  if(state == ENABLE)
  {
    __nRF24L01.config &= ~(interrupt & nRF_IT_MASK);
  }
  else
  {
    __nRF24L01.config |= (interrupt & nRF_IT_MASK);
  }

  SPI_WR_Reg(WRITE_nRF_REG + CONFIG, __nRF24L01.config);
  nRF24L01_Delay_us(5);
}

//***************************************************//
//Define the other function

void nRF24L01_SetTxAddress(unsigned char * addr)
{
  int i;
  for( i = 0; i < __nRF24L01.adr_width; i++)
  {
    __nRF24L01.tx_addr[i] = addr[i];
  }

  // Write address into tx_add
  SPI_Write_Buf(WRITE_nRF_REG + TX_ADDR, __nRF24L01.tx_addr, __nRF24L01.adr_width);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetRxAddress0(unsigned char * addr)
{
  int i;
  for( i = 0; i < __nRF24L01.adr_width; i++)
  {
    __nRF24L01.rx_addr[i] = addr[i];
  }

  // Write address into rx_add_p0
  SPI_Write_Buf(WRITE_nRF_REG + RX_ADDR_P0, __nRF24L01.rx_addr, __nRF24L01.adr_width);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetRxAddress1(unsigned char * addr)
{
  int i;
  for( i = 0; i < __nRF24L01.adr_width; i++)
  {
    __nRF24L01.rx_addr1[i] = addr[i];
  }

  // Write address into rx_add_p1
  SPI_Write_Buf(WRITE_nRF_REG + RX_ADDR_P1, __nRF24L01.rx_addr1, __nRF24L01.adr_width);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetRxAddress2(unsigned char * addr)
{
  __nRF24L01.rx_addr2[0] = addr[0];

  // Write address into rx_add_p2
  SPI_Write_Buf(WRITE_nRF_REG + RX_ADDR_P2, __nRF24L01.rx_addr2, 1);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetRxAddress3(unsigned char * addr)
{
  __nRF24L01.rx_addr3[0] = addr[0];

  // Write address into rx_add_p3
  SPI_Write_Buf(WRITE_nRF_REG + RX_ADDR_P3, __nRF24L01.rx_addr3, 1);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetRxAddress4(unsigned char * addr)
{
  __nRF24L01.rx_addr4[0] = addr[0];

  // Write address into rx_add_p4
  SPI_Write_Buf(WRITE_nRF_REG + RX_ADDR_P4, __nRF24L01.rx_addr4, 1);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetRxAddress5(unsigned char * addr)
{
  __nRF24L01.rx_addr5[0] = addr[0];

  // Write address into rx_add_p5
  SPI_Write_Buf(WRITE_nRF_REG + RX_ADDR_P5, __nRF24L01.rx_addr5, 1);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetChannel(unsigned char channel)
{
  // Setup channel
  __nRF24L01.channel = channel;
  SPI_WR_Reg(WRITE_nRF_REG + RF_CH,__nRF24L01.channel);
  nRF24L01_Delay_us(5);
}



void nRF24L01_RxAddress(unsigned char pipe, FunctionalState state)
{
  if((pipe >= 0)&&(pipe <= 5))
  {
    if(state == ENABLE)
    {
      __nRF24L01.en_rxaddr |= (1<<pipe);
    }
    else
    {
      __nRF24L01.en_rxaddr &= ~(1<<pipe);
    }
    
    // Enable rx address
    SPI_WR_Reg(WRITE_nRF_REG + EN_RXADDR, __nRF24L01.en_rxaddr);
    nRF24L01_Delay_us(5);
  }
}

void nRF24L01_AutoAck(unsigned char pipe, FunctionalState state)
{
  if((pipe >= 0)&&(pipe <= 5))
  {
    if(state == ENABLE)
    {
      __nRF24L01.en_aa |= (1<<pipe);
    }
    else
    {
      __nRF24L01.en_aa &= ~(1<<pipe);
    }
    
    SPI_WR_Reg(WRITE_nRF_REG + EN_AA, __nRF24L01.en_aa);
    nRF24L01_Delay_us(5);
  }
}

void nRF24L01_EnableDynamicPayload(FunctionalState state)
{
  if(state == ENABLE)
  {
    __nRF24L01.feature |= FEATURE_EN_DPL;
  }
  else
  {
    __nRF24L01.feature &= ~FEATURE_EN_DPL;
  }

  SPI_WR_Reg(WRITE_nRF_REG + FEATURE, __nRF24L01.feature);
  nRF24L01_Delay_us(5);
}

void nRF24L01_EnableAckPayload(FunctionalState state)
{
  if(state == ENABLE)
  {
    __nRF24L01.feature |= FEATURE_EN_ACK_PAY;
  }
  else
  {
    __nRF24L01.feature &= FEATURE_EN_ACK_PAY;
  }

  SPI_WR_Reg(WRITE_nRF_REG + FEATURE, __nRF24L01.feature);
  nRF24L01_Delay_us(5);
}

void nRF24L01_DynamicPayload(unsigned char pipe, FunctionalState state)
{
  if((pipe >= 0)&&(pipe <= 5))
  {
    if(state == ENABLE)
    {
      __nRF24L01.dynpd |= (1<<pipe);
    }
    else
    {
      __nRF24L01.dynpd &= ~(1<<pipe);
    }

    SPI_WR_Reg(WRITE_nRF_REG + DYNPD, __nRF24L01.dynpd);
    nRF24L01_Delay_us(5);
  }
}

unsigned char nRF24L01_GetLastPayloadSize(void)
{
  return SPI_RD_Reg(R_RX_PL_WID);
}

void nRF24L01_SetPayloadWidth(unsigned char pipe, unsigned char ploadw)
{
  if((pipe >= 0)&&(pipe <= 5))
  {
    // Payload width setup
    __nRF24L01.pload_width[pipe] = ploadw;
    SPI_WR_Reg(WRITE_nRF_REG + RX_PW_P0+pipe, __nRF24L01.pload_width[pipe]);
  }
}

void nRF24L01_SetAutoRetransmitDelay(unsigned char ard)
{
  // Setup Auto Retransmit Delay
  __nRF24L01.setup_retr = (ard & SETUP_RETR_ARD) | (__nRF24L01.setup_retr & ~SETUP_RETR_ARD);
  SPI_WR_Reg(WRITE_nRF_REG + SETUP_RETR, __nRF24L01.setup_retr);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetAutoRetransmitCount(unsigned char arc)
{
  // Setup Auto Retransmit Count
  __nRF24L01.setup_retr = (arc & SETUP_RETR_ARC) | (__nRF24L01.setup_retr & ~SETUP_RETR_ARC);
  SPI_WR_Reg(WRITE_nRF_REG + SETUP_RETR, __nRF24L01.setup_retr);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetTXPower(unsigned char power)
{
  //Setup TX Power
  __nRF24L01.rf_setup = (power & RF_SETUP_RF_PWR) | (__nRF24L01.rf_setup & ~RF_SETUP_RF_PWR);
  SPI_WR_Reg(WRITE_nRF_REG + RF_SETUP ,__nRF24L01.rf_setup);
  nRF24L01_Delay_us(5);
}

void nRF24L01_SetDataRate(unsigned char rate)
{
  //Setup Data Rate
  __nRF24L01.rf_setup = (rate & RF_SETUP_RF_DR) | (__nRF24L01.rf_setup & ~RF_SETUP_RF_DR);
  SPI_WR_Reg(WRITE_nRF_REG + RF_SETUP ,__nRF24L01.rf_setup);
  nRF24L01_Delay_us(5);
}

unsigned char nRF24L01_ReceivedPowerDetector(void)
{
	__nRF24L01.rpd = SPI_RD_Reg(RPD);
	return __nRF24L01.rpd;
}
