#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_spi.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_dma.h"

//***************************************************//
// GPIO for SPI

//#define SPI                   		SPI1
#define SPI_nrf											SPI1

#define GPIO_IRQnrf                  	GPIOA
#define GPIO_Pin_IRQnrf              	GPIO_Pin_4
#define RCC_AHBPeriph_GPIO_IRQnrf   	RCC_AHBPeriph_GPIOA
#define EXTI_PortSourceGPIO_IRQnrf    EXTI_PortSourceGPIOA
#define EXTI_PinSource_IRQnrf         EXTI_PinSource4
#define EXTI_IRQnrf                   EXTI4_15_IRQn
#define EXTI_Linenrf                  EXTI_Line4

#define GPIO_CS_CE                GPIOA
#define GPIO_Pin_CE              	GPIO_Pin_3
#define GPIO_Pin_CS              	GPIO_Pin_2
#define RCC_AHBPeriph_GPIO_CS_CE  RCC_AHBPeriph_GPIOA

#define GPIO_SPI              		GPIOA
#define GPIO_AF_SPI           		GPIO_AF_0
#define GPIO_Pin_SPI_SCK      		GPIO_Pin_5
#define GPIO_Pin_SPI_MISO     		GPIO_Pin_6
#define GPIO_Pin_SPI_MOSI     		GPIO_Pin_7
#define RCC_APBPeriph_SPI     		RCC_APB2Periph_SPI1
#define GPIO_Pin_SPI_SCK_SOURCE         GPIO_PinSource5
#define GPIO_Pin_SPI_MISO_SOURCE        GPIO_PinSource6
#define GPIO_Pin_SPI_MOSI_SOURCE        GPIO_PinSource7
#define RCC_AHBPeriph_GPIO_SPI          RCC_AHBPeriph_GPIOA

#define nRF24L01_IRQHandler             EXTI4_15_IRQHandler

//***************************************************//
// DMA for SPI

#define SPI_nrf_DMA										DMA1

#define SPI_nrf_DR_ADDRESS						&SPI_nrf->DR

#define SPI_nrf_RX_DMA_CHANNEL				DMA1_Channel2
#define SPI_nrf_RX_DMA_FLAG_TC				DMA1_FLAG_TC2
#define SPI_nrf_RX_DMA_FLAG_GL				DMA1_FLAG_GL2

#define SPI_nrf_TX_DMA_CHANNEL				DMA1_Channel3
#define SPI_nrf_TX_DMA_FLAG_TC				DMA1_FLAG_TC3
#define SPI_nrf_TX_DMA_FLAG_GL				DMA1_FLAG_GL3

#define RCC_AHBPeriph_DMA							RCC_AHBPeriph_DMA1

//***************************************************//
// Options

#define NRF_DMA_ENABLE
#define NRF_SPI_OVERCLOK

//***************************************************//
//Define the layer1:HW operation

void nRF24L01_HW_SPI_Init(void);
void nRF24L01_HW_EXTI_Init(void);
void nRF24L01_HW_DMA_Init(void);
void nRF24L01_HW_Init(void);

void nRF24L01_SPI_NSS_L(void);
void nRF24L01_SPI_NSS_H(void);
void nRF24L01_CE_L(void);
void nRF24L01_CE_H(void);
unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat);
void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len);
void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len);

void nRF24L01_Delay_us(unsigned long n);