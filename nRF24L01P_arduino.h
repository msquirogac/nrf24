#include "Arduino.h"
#include <inttypes.h>


#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

//***************************************************//
// GPIO for SPI

#define GPIO_IRQnrf                  	GPIOA
#define GPIO_Pin_IRQnrf              	GPIO_Pin_2
#define RCC_AHBPeriph_GPIO_IRQnrf   	RCC_AHB1Periph_GPIOA
#define EXTI_PortSourceGPIO_IRQnrf      EXTI_PortSourceGPIOA
#define EXTI_PinSource_IRQnrf           EXTI_PinSource2
#define EXTI_IRQnrf                     EXTI2_IRQn
#define EXTI_Linenrf                    EXTI_Line2

#define GPIO_Pin_CE              	9
#define GPIO_Pin_CS              	8

#define GPIO_Pin_SPI_SCK      		SCK
#define GPIO_Pin_SPI_MISO     		MISO
#define GPIO_Pin_SPI_MOSI     		MOSI

#define nRF24L01_IRQHandler             EXTI2_IRQHandler

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;

//***************************************************//
//Define the layer1:HW operation

void nRF24L01_HW_SPI_Init(void);
void nRF24L01_HW_EXTI_Init(void);
void nRF24L01_HW_Init(void);

void nRF24L01_SPI_NSS_L(void);
void nRF24L01_SPI_NSS_H(void);
void nRF24L01_CE_L(void);
void nRF24L01_CE_H(void);
unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat);
void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len);
void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len);

void nRF24L01_Delay_us(unsigned int n);

#ifdef __cplusplus
}
#endif /* __cplusplus */
