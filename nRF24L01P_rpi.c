#ifdef RASPBERRY

#include "nRF24L01P_rpi.h"

//***************************************************//
//Define the layer1 functions

void nRF24L01_HW_SPI_Init(void)
{
  if(wiringPiSetupGpio() < 0)
  {
    fprintf (stderr, "Setup failed: %s\n", strerror (errno));
    exit(1);
  }

  /* Configure CE pin */
  pinMode(GPIO_Pin_CE, OUTPUT);
	
  /* Configure CS pin */
  pinMode(GPIO_Pin_CS, OUTPUT);

  /* SPI configuration */
  if (wiringPiSPISetup (SPI_CHANNEL, SPI_SPEED) < 0)
  {
    fprintf (stderr, "SPI Setup failed: %s\n", strerror (errno));
    exit(1);
  }
}

void nRF24L01_HW_EXTI_Init(void)
{
  /* Configure IRQ pin */
  if(wiringPiISR(GPIO_Pin_IRQ, INT_EDGE_FALLING, &nRF24L01_IRQHandler) < 0)
  {
    fprintf (stderr, "Unable to setup ISR: %s\n", strerror (errno)) ;
    exit(1);
  }
}

void nRF24L01_HW_Init(void)
{
  nRF24L01_HW_SPI_Init();
  nRF24L01_HW_EXTI_Init();
}

void nRF24L01_SPI_NSS_L(void)
{
  digitalWrite(GPIO_Pin_CS,LOW);
}

void nRF24L01_SPI_NSS_H(void)
{
  digitalWrite(GPIO_Pin_CS,HIGH);
}

void nRF24L01_CE_L(void)
{
  digitalWrite(GPIO_Pin_CE,LOW);
}

void nRF24L01_CE_H(void)
{
  digitalWrite(GPIO_Pin_CE,HIGH);
}

unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat)
{
  wiringPiSPIDataRW(SPI_CHANNEL, &dat, 1);
  return dat;
}

void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i, BufTmp[40];

  for(i=0;i<Len;i++)
  {
     BufTmp[i] = pBuf[i];
  }

  wiringPiSPIDataRW(SPI_CHANNEL, BufTmp, Len);
}

void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i, BufTmp[40];

  wiringPiSPIDataRW(SPI_CHANNEL, BufTmp, Len);
  for(i=0;i<Len;i++)
  {
     pBuf[i] = BufTmp[i];
  }
}

void nRF24L01_Delay_us(unsigned long n)
{
  delayMicroseconds(n);
}

#endif
