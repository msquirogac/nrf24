#include "stm32f30x_rcc.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_spi.h"
#include "stm32f30x_exti.h"
#include "stm32f30x_misc.h"
#include "stm32f30x_dma.h"

//***************************************************//
// GPIO for SPI

#define GPIO_IRQnrf										GPIOD
#define GPIO_Pin_IRQnrf								GPIO_Pin_0
#define RCC_AHBPeriph_GPIO_IRQnrf			RCC_AHBPeriph_GPIOD
#define EXTI_PortSourceGPIO_IRQnrf		EXTI_PortSourceGPIOD
#define EXTI_PinSource_IRQnrf					EXTI_PinSource0
#define EXTI_IRQnrf										EXTI0_IRQn
#define EXTI_Linenrf									EXTI_Line0

#define GPIO_CS_CE										GPIOD
#define GPIO_Pin_CE										GPIO_Pin_1
#define GPIO_Pin_CS										GPIO_Pin_2
#define RCC_AHBPeriph_GPIO_CS_CE			RCC_AHBPeriph_GPIOD

#define SPI_nrf												SPI3
#define GPIO_SPI											GPIOC
#define GPIO_AF_SPI										GPIO_AF_6
#define GPIO_Pin_SPI_SCK							GPIO_Pin_10
#define GPIO_Pin_SPI_MISO							GPIO_Pin_11
#define GPIO_Pin_SPI_MOSI							GPIO_Pin_12
#define RCC_APBPeriph_SPI							RCC_APB1Periph_SPI3
#define GPIO_Pin_SPI_SCK_SOURCE				GPIO_PinSource10
#define GPIO_Pin_SPI_MISO_SOURCE			GPIO_PinSource11
#define GPIO_Pin_SPI_MOSI_SOURCE			GPIO_PinSource12
#define RCC_AHBPeriph_GPIO_SPI				RCC_AHBPeriph_GPIOC

#define nRF24L01_IRQHandler						EXTI0_IRQHandler

//***************************************************//
// DMA for SPI

#define SPI_nrf_DMA										DMA2

#define SPI_nrf_DR_ADDRESS						&SPI_nrf->DR

#define SPI_nrf_RX_DMA_CHANNEL				DMA2_Channel1
#define SPI_nrf_RX_DMA_FLAG_TC				DMA2_FLAG_TC1
#define SPI_nrf_RX_DMA_FLAG_GL				DMA2_FLAG_GL1

#define SPI_nrf_TX_DMA_CHANNEL				DMA2_Channel2
#define SPI_nrf_TX_DMA_FLAG_TC				DMA2_FLAG_TC2
#define SPI_nrf_TX_DMA_FLAG_GL				DMA2_FLAG_GL2

#define RCC_AHBPeriph_DMA							RCC_AHBPeriph_DMA2

//***************************************************//
// Options

#define NRF_DMA_ENABLE
#define NRF_SPI_OVERCLOK

//***************************************************//
//Define the layer1:HW operation

void nRF24L01_HW_SPI_Init(void);
void nRF24L01_HW_EXTI_Init(void);
void nRF24L01_HW_DMA_Init(void);
void nRF24L01_HW_Init(void);

void nRF24L01_SPI_NSS_L(void);
void nRF24L01_SPI_NSS_H(void);
void nRF24L01_CE_L(void);
void nRF24L01_CE_H(void);
unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat);
void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len);
void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len);

void nRF24L01_Delay_us(unsigned long n);