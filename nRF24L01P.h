#include "nRF24L01P_API.h"
#include "nRF24L01P_types.h"

#if defined (STM32F051)
  #include "nRF24L01P_stm32F0xx.h"
#elif  defined (STM32F4XX)
  #include "nRF24L01P_stm32F4xx.h"
#elif  defined (STM32F30X)
  #include "nRF24L01P_stm32F30x.h"
#elif  defined (RASPBERRY)
  #include "nRF24L01P_rpi.h"
#elif  defined (ARDUINO)
  #include "nRF24L01P_arduino.h"
#elif  defined (LPC1114FN28)
  #include "nRF24L01P_lpc1114fn28.h"
#endif


#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

//***************************************************//
//Define the layer2:Reg operation
unsigned char SPI_SR_Reg(void);
unsigned char SPI_RD_Reg(unsigned char reg);
unsigned char SPI_WR_Reg(unsigned char reg, unsigned char value);
unsigned char SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char Len);
unsigned char SPI_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned char Len);

//***************************************************//
//Define the layer3:application operation
void nRF24L01_Init(void);
void nRF24L01_PowerRadio(FunctionalState);
void nRF24L01_Cmd(FunctionalState);
void nRF24L01_Setup(void);

void nRF24L01_RX_Mode(void);
void nRF24L01_TX_Mode(void);

unsigned char nRF24L01_RxPacket(nRF_RXDataTypeDef* RX_Packet, FunctionalState dynamic);
void nRF24L01_TxPacket(nRF_TXDataTypeDef* TX_Packet);

unsigned char nRF24L01_RxPacketNb(nRF_RXDataTypeDef* RX_Packet, FunctionalState dynamic);
void nRF24L01_TxPacketNb(nRF_TXDataTypeDef* TX_Packet);

void nRF24L01_TxPacketAck(nRF_TXDataTypeDef* TX_Packet, unsigned char pipe);

unsigned char nRF24L01_TXSuccess(void);
unsigned char nRF24L01_RXSuccess(void);
void nRF24L01_TxFlush(void);
void nRF24L01_RxFlush(void);

int8_t nRF24L01_Status(void);
int8_t nRF24L01_GetStatus(void);

int8_t nRF24L01_FifoStatus(void);
int8_t nRF24L01_GetFifoStatus(void);

int8_t nRF24L01_ITStatus(void);
int8_t nRF24L01_GetITStatus(int8_t irq);
void nRF24L01_ClearITPendingBit(int8_t irq);
void nRF24L01_ITConfig(unsigned char interrupt, FunctionalState state);

//***************************************************//
//Define the other function
void nRF24L01_SetTxAddress(unsigned char * addr);
void nRF24L01_SetRxAddress0(unsigned char * addr);
void nRF24L01_SetRxAddress1(unsigned char * addr);
void nRF24L01_SetRxAddress2(unsigned char * addr);
void nRF24L01_SetRxAddress3(unsigned char * addr);
void nRF24L01_SetRxAddress4(unsigned char * addr);
void nRF24L01_SetRxAddress5(unsigned char * addr);

void nRF24L01_SetChannel(unsigned char channel);

void nRF24L01_RxAddress(unsigned char pipe, FunctionalState state);
void nRF24L01_AutoAck(unsigned char pipe, FunctionalState state);
void nRF24L01_EnableDynamicPayload(FunctionalState state);
void nRF24L01_EnableAckPayload(FunctionalState state);

void nRF24L01_DynamicPayload(unsigned char pipe, FunctionalState state);

unsigned char nRF24L01_GetLastPayloadSize(void);
void nRF24L01_SetPayloadWidth(unsigned char pipe, unsigned char ploadw);

void nRF24L01_SetAutoRetransmitDelay(unsigned char ard);
void nRF24L01_SetAutoRetransmitCount(unsigned char arc);
void nRF24L01_SetTXPower(unsigned char power);
void nRF24L01_SetDataRate(unsigned char rate);

unsigned char nRF24L01_ReceivedPowerDetector(void);
#ifdef __cplusplus
}
#endif /* __cplusplus */
