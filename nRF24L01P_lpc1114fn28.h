#include <LPC11xx.h>
#include "gpio.h"
#include "ssp.h"

//***************************************************//
// GPIO for SPI

#define SPI                   		0

#define GPIO_IRQnrf                  	GPIOA
#define GPIO_Pin_IRQnrf              	GPIO_Pin_2
#define RCC_AHBPeriph_GPIO_IRQnrf   	RCC_AHB1Periph_GPIOA
#define EXTI_PortSourceGPIO_IRQnrf      EXTI_PortSourceGPIOA
#define EXTI_PinSource_IRQnrf           EXTI_PinSource2
#define EXTI_IRQnrf                     EXTI2_IRQn
#define EXTI_Linenrf                    EXTI_Line2

#define GPIO_CS_CE                  	PORT0
#define GPIO_Pin_CE              	3
#define GPIO_Pin_CS              	7

#define GPIO_SPI              		GPIOA
#define GPIO_AF_SPI           		GPIO_AF_SPI1
#define GPIO_Pin_SPI_SCK      		GPIO_Pin_5
#define GPIO_Pin_SPI_MISO     		GPIO_Pin_6
#define GPIO_Pin_SPI_MOSI     		GPIO_Pin_7
#define RCC_APBPeriph_SPI     		RCC_APB2Periph_SPI1
#define GPIO_Pin_SPI_CS_SOURCE          GPIO_PinSource4
#define GPIO_Pin_SPI_SCK_SOURCE         GPIO_PinSource5
#define GPIO_Pin_SPI_MISO_SOURCE        GPIO_PinSource6
#define GPIO_Pin_SPI_MOSI_SOURCE        GPIO_PinSource7
#define RCC_AHBPeriph_GPIO_SPI          RCC_AHB1Periph_GPIOA

#define nRF24L01_IRQHandler             EXTI2_IRQHandler

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;

//***************************************************//
//Define the layer1:HW operation

void nRF24L01_HW_SPI_Init(void);
void nRF24L01_HW_EXTI_Init(void);
void nRF24L01_HW_Init(void);

void nRF24L01_SPI_NSS_L(void);
void nRF24L01_SPI_NSS_H(void);
void nRF24L01_CE_L(void);
void nRF24L01_CE_H(void);
unsigned char nRF24L01_SPI_Send_Byte(unsigned char dat);

void nRF24L01_Delay_us(unsigned long n);