#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>

//***************************************************//
// GPIO for SPI
#define SPI_CHANNEL                     0
#define SPI_SPEED                       8000000

#define EXTI_IRQnrf                     EXTI2_3_IRQn
#define EXTI_Linenrf                    EXTI_Line2

#define GPIO_Pin_IRQ              	25

#define GPIO_Pin_CE              	7
#define GPIO_Pin_CS              	8

#define GPIO_Pin_SPI_SCK      		11
#define GPIO_Pin_SPI_MISO     		9
#define GPIO_Pin_SPI_MOSI     		10

//#define nRF24L01_IRQHandler

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;

//***************************************************//
//Define the layer1:HW operation

void nRF24L01_HW_SPI_Init(void);
void nRF24L01_HW_EXTI_Init(void);
void nRF24L01_HW_Init(void);

void nRF24L01_SPI_NSS_L(void);
void nRF24L01_SPI_NSS_H(void);
void nRF24L01_CE_L(void);
void nRF24L01_CE_H(void);
unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat);
void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len);
void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len);

void nRF24L01_Delay_us(unsigned long n);

extern void nRF24L01_IRQHandler(void);

