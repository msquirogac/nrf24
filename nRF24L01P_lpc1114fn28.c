#ifdef LPC1114FN28

#include "nRF24L01P_lpc1114fn28.h"

//***************************************************//
//Define the layer1 functions

void nRF24L01_HW_SPI_Init(void)
{
  /* GPIO Config */
  GPIOInit();

  /* Configure CE pin */
  GPIOSetDir( GPIO_CS_CE, GPIO_Pin_CE, 1);
	
  /* Configure CS pin */
  GPIOSetDir( GPIO_CS_CE, GPIO_Pin_CS, 1);

  /* SPI configuration */
  SSP_IOConfig(SPI);
  SSP_Init(SPI);
}

void nRF24L01_HW_EXTI_Init(void)
{

}

void nRF24L01_HW_Init(void)
{
  nRF24L01_HW_SPI_Init();
  nRF24L01_HW_EXTI_Init();
}

void nRF24L01_SPI_NSS_L(void)
{
  GPIOSetValue( GPIO_CS_CE, GPIO_Pin_CS, 0);
}

void nRF24L01_SPI_NSS_H(void)
{
  GPIOSetValue( GPIO_CS_CE, GPIO_Pin_CS, 1);
}

void nRF24L01_CE_L(void)
{
  GPIOSetValue( GPIO_CS_CE, GPIO_Pin_CE, 0);
}

void nRF24L01_CE_H(void)
{
  GPIOSetValue( GPIO_CS_CE, GPIO_Pin_CE, 1);
}

unsigned char nRF24L01_SPI_Send_Byte(unsigned char dat)
{
  /* Move on only if NOT busy and TX FIFO not full. */
  while ( (LPC_SSP0->SR & (SSPSR_TNF|SSPSR_BSY)) != SSPSR_TNF );
  LPC_SSP0->DR = dat;

  /* Wait until the Busy bit is cleared */
  while ( (LPC_SSP0->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );

  return LPC_SSP0->DR;
}

void __attribute__((optimize("O0"))) nRF24L01_Delay_us(unsigned long n)
{
  for(n=n*4-3; n>0; n--);
}}

#endif