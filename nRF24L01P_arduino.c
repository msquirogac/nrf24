#ifdef ARDUINO

#include "nRF24L01P_arduino.h"

//***************************************************//
//Define the layer1 functions

void nRF24L01_HW_SPI_Init(void)
{
  /* Configure CE pin */
  pinMode(GPIO_Pin_CE, OUTPUT);
	
  /* Configure CS pin */
  pinMode(GPIO_Pin_CS, OUTPUT);

  /* SPI configuration */
  pinMode(SCK, OUTPUT);
  pinMode(MOSI, OUTPUT);
  pinMode(SS, OUTPUT);
  
  digitalWrite(SCK, LOW);
  digitalWrite(MOSI, LOW);
  digitalWrite(SS, HIGH);

  // Warning: if the SS pin ever becomes a LOW INPUT then SPI 
  // automatically switches to Slave, so the data direction of 
  // the SS pin MUST be kept as OUTPUT.
  SPCR |= _BV(MSTR);
  SPCR |= _BV(SPE);
}

void nRF24L01_HW_EXTI_Init(void)
{

}

void nRF24L01_HW_Init(void)
{
  nRF24L01_HW_SPI_Init();
  nRF24L01_HW_EXTI_Init();
}

void nRF24L01_SPI_NSS_L(void)
{
  digitalWrite(GPIO_Pin_CS,LOW);
}

void nRF24L01_SPI_NSS_H(void)
{
  digitalWrite(GPIO_Pin_CS,HIGH);
}

void nRF24L01_CE_L(void)
{
  digitalWrite(GPIO_Pin_CE,LOW);
}

void nRF24L01_CE_H(void)
{
  digitalWrite(GPIO_Pin_CE,HIGH);
}

unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat)
{
  SPDR = dat;
  while (!(SPSR & _BV(SPIF)))
    ;
  return SPDR;
}

void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i;

  for(i=0; i<Len; i++)
  {
    nRF24L01_SPI_SendRecv_Byte(*pBuf);
    pBuf++;
  }
}

void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i;

  for(i=0;i<Len;i++)
  {
     pBuf[i] = nRF24L01_SPI_SendRecv_Byte(0xFF);
  }
}

void nRF24L01_Delay_us(unsigned int n)
{
  delayMicroseconds(n);
}

#endif
