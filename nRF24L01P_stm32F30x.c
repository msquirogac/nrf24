#ifdef STM32F30X

#include "nRF24L01P_stm32F30x.h"

//***************************************************//
//Define the layer1 functions

void nRF24L01_HW_SPI_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef  SPI_InitStructure;

  /* Enable SPI GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIO_SPI, ENABLE);
  /* Enable SPI Periph clock */
  RCC_APB1PeriphClockCmd(RCC_APBPeriph_SPI, ENABLE);
  /* Enable GPIO of CHIP SELECT */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIO_CS_CE, ENABLE);
	
  /* Connect SPI pins to AF */
  GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_SCK_SOURCE,GPIO_AF_SPI);
  GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_MOSI_SOURCE,GPIO_AF_SPI);
  GPIO_PinAFConfig(GPIO_SPI, GPIO_Pin_SPI_MISO_SOURCE,GPIO_AF_SPI);
	
  /* Configure SPI pins:  SCK ,MOSI, MISO */
  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_SPI_SCK | GPIO_Pin_SPI_MOSI | GPIO_Pin_SPI_MISO;
  GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_Init(GPIO_SPI, &GPIO_InitStructure);

  /* Configure CS pin */
  //SPI_SSOutputCmd(SPI, ENABLE);
  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_CS|GPIO_Pin_CE;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_DOWN;
  GPIO_Init(GPIO_CS_CE, &GPIO_InitStructure);

  /* SPI configuration */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;

  #ifdef NRF_SPI_OVERCLOK
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
	#else
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
	#endif

  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(SPI_nrf, &SPI_InitStructure);

  SPI_CalculateCRC(SPI_nrf, DISABLE);

  SPI_RxFIFOThresholdConfig(SPI_nrf, SPI_RxFIFOThreshold_QF);

  /* SPI nrf enable */
  SPI_Cmd(SPI_nrf, ENABLE);
}

void nRF24L01_HW_EXTI_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable GPIO of EXTI */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIO_IRQnrf, ENABLE);
  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Configure EXTI pin */
  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_IRQnrf;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd =  GPIO_PuPd_NOPULL;
  GPIO_Init(GPIO_IRQnrf, &GPIO_InitStructure);

  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIO_IRQnrf, EXTI_PinSource_IRQnrf);
  
  EXTI_StructInit(&EXTI_InitStructure);
  EXTI_InitStructure.EXTI_Line = EXTI_Linenrf;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  NVIC_InitStructure.NVIC_IRQChannel = EXTI_IRQnrf;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x07;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

#ifdef NRF_DMA_ENABLE
void nRF24L01_HW_DMA_Init(void)
{
	DMA_InitTypeDef DMA_InitStructure;

  /* Enable DMA clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA, ENABLE);

	/* DMA channel Rx of SPI Configuration */
  DMA_DeInit(SPI_nrf_RX_DMA_CHANNEL);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)SPI_nrf_DR_ADDRESS;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) 0x00;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = (uint16_t)32;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(SPI_nrf_RX_DMA_CHANNEL, &DMA_InitStructure);
   
	/* DMA channel Tx of SPI Configuration */
  DMA_DeInit(SPI_nrf_TX_DMA_CHANNEL);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)SPI_nrf_DR_ADDRESS;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) 0x00;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = (uint16_t)32;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(SPI_nrf_TX_DMA_CHANNEL, &DMA_InitStructure);

  /* Enable the SPI Rx & Tx DMA request */
	SPI_I2S_DMACmd(SPI_nrf, SPI_I2S_DMAReq_Rx, ENABLE);
  SPI_I2S_DMACmd(SPI_nrf, SPI_I2S_DMAReq_Tx, ENABLE);

	/* Disable DMA nRF RX & TX*/
	DMA_Cmd(SPI_nrf_RX_DMA_CHANNEL, DISABLE);
  DMA_Cmd(SPI_nrf_TX_DMA_CHANNEL, DISABLE);
}
#endif

void nRF24L01_HW_Init(void)
{
  nRF24L01_HW_SPI_Init();
  nRF24L01_HW_EXTI_Init();

#ifdef NRF_DMA_ENABLE
  nRF24L01_HW_DMA_Init();
#endif
}

void nRF24L01_SPI_NSS_L(void)
{
  GPIO_ResetBits(GPIO_CS_CE, GPIO_Pin_CS);
}

void nRF24L01_SPI_NSS_H(void)
{
  GPIO_SetBits(GPIO_CS_CE, GPIO_Pin_CS);
}

void nRF24L01_CE_L(void)
{
  GPIO_ResetBits(GPIO_CS_CE, GPIO_Pin_CE);
}

void nRF24L01_CE_H(void)
{
  GPIO_SetBits(GPIO_CS_CE, GPIO_Pin_CE);
}

unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat)
{
  /* Loop while DR register in not emplty */
  while(SPI_I2S_GetFlagStatus(SPI_nrf, SPI_I2S_FLAG_TXE) == RESET);

  /* Send byte through the SPIx peripheral */
  //SPI_I2S_SendData16(SPI, dat);
  SPI_SendData8(SPI_nrf, dat);

  /* Wait to receive a byte */
  while(SPI_I2S_GetFlagStatus(SPI_nrf, SPI_I2S_FLAG_RXNE) == RESET);

  /* Return the byte read from the SPI bus */
  return SPI_ReceiveData8(SPI_nrf);
}

#ifdef NRF_DMA_ENABLE

#define NRF_SPI_TIMEOUT 1000
void nRF24L01_SPI_DMA_Start(void)
{
	if(DMA_GetCurrDataCounter(SPI_nrf_TX_DMA_CHANNEL) && DMA_GetCurrDataCounter(SPI_nrf_RX_DMA_CHANNEL))
	{
		/* Enable DMA Tx & Rx*/
		DMA_Cmd(SPI_nrf_TX_DMA_CHANNEL, ENABLE);
		DMA_Cmd(SPI_nrf_RX_DMA_CHANNEL, ENABLE);
  
		/* Enable the SPI Tx & Rx DMA request */
		SPI_I2S_DMACmd(SPI_nrf, SPI_I2S_DMAReq_Tx, ENABLE);
		SPI_I2S_DMACmd(SPI_nrf, SPI_I2S_DMAReq_Rx, ENABLE);

		/* Waiting the end of Data transfer */
		int32_t TimeOut;
		/*
		TimeOut = NRF_SPI_TIMEOUT;
		while((DMA_GetFlagStatus(SPI_nrf_TX_DMA_FLAG_TC) == RESET)&&(TimeOut-- != 0x00));
		*/
		TimeOut = NRF_SPI_TIMEOUT;
		while((DMA_GetFlagStatus(SPI_nrf_RX_DMA_FLAG_TC) == RESET)&&(TimeOut-- != 0x00));
	}

	/* Clear DMA flags */
	DMA_ClearFlag(SPI_nrf_TX_DMA_FLAG_TC);
	DMA_ClearFlag(SPI_nrf_RX_DMA_FLAG_TC);

	/* Disable DMA nRF Tx & Rx */
	DMA_Cmd(SPI_nrf_TX_DMA_CHANNEL, DISABLE);
	DMA_Cmd(SPI_nrf_RX_DMA_CHANNEL, DISABLE);

  /* Disable the SPI Tx & Rx DMA request */
	SPI_I2S_DMACmd(SPI_nrf, SPI_I2S_DMAReq_Tx, DISABLE);
	SPI_I2S_DMACmd(SPI_nrf, SPI_I2S_DMAReq_Rx, DISABLE);
}

void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len)
{
	int8_t RX;

	/* Disable memory incremented */
	SPI_nrf_RX_DMA_CHANNEL->CCR &= ~DMA_MemoryInc_Enable;

	/* Change remaining bytes register */
	SPI_nrf_RX_DMA_CHANNEL->CNDTR = Len;

	/* Change memory address register */
	SPI_nrf_RX_DMA_CHANNEL->CMAR = (uint32_t)&RX;


	/* Enable memory incremented */
	SPI_nrf_TX_DMA_CHANNEL->CCR |= DMA_MemoryInc_Enable;

	/* Change remaining bytes register */
	SPI_nrf_TX_DMA_CHANNEL->CNDTR = Len;

	/* Change memory address register */
	SPI_nrf_TX_DMA_CHANNEL->CMAR = (uint32_t)pBuf;

	nRF24L01_SPI_DMA_Start();
}

void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len)
{
	int8_t TX = 0;

	/* Enable memory incremented */
	SPI_nrf_RX_DMA_CHANNEL->CCR |= DMA_MemoryInc_Enable;

	/* Change remaining bytes register */
	SPI_nrf_RX_DMA_CHANNEL->CNDTR = Len;

	/* Change memory address register */
	SPI_nrf_RX_DMA_CHANNEL->CMAR = (uint32_t)pBuf;

	
	/* Disable memory incremented */
	SPI_nrf_TX_DMA_CHANNEL->CCR &= ~DMA_MemoryInc_Enable;

	/* Change remaining bytes register */
	SPI_nrf_TX_DMA_CHANNEL->CNDTR = Len;

	/* Change memory address register */
	SPI_nrf_TX_DMA_CHANNEL->CMAR = (uint32_t)&TX;

	nRF24L01_SPI_DMA_Start();
}

#else

void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i;

  for(i=0; i<Len; i++)
  {
    nRF24L01_SPI_SendRecv_Byte(*pBuf);
    pBuf++;
  }
}

void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len)
{
  unsigned char i;

  for(i=0;i<Len;i++)
  {
     pBuf[i] = nRF24L01_SPI_SendRecv_Byte(0xFF);
  }
}

#endif

void __attribute__((optimize("O0"))) nRF24L01_Delay_us(unsigned long n)
{
  for(n=n*8-6; n>0; n--);
}

#endif