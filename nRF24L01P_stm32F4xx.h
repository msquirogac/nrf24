#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "misc.h"

//***************************************************//
// GPIO Config

#define GPIO_IRQnrf                  	GPIOD
#define GPIO_Pin_IRQnrf              	GPIO_Pin_7
#define RCC_AHBPeriph_GPIO_IRQnrf   	RCC_AHB1Periph_GPIOD
#define EXTI_PortSourceGPIO_IRQnrf      EXTI_PortSourceGPIOD
#define EXTI_PinSource_IRQnrf           EXTI_PinSource7
#define EXTI_IRQnrf                     EXTI9_5_IRQn
#define EXTI_Linenrf                    EXTI_Line7
#define nRF24L01_IRQHandler             EXTI9_5_IRQHandler

#define GPIO_CS_CE                  	GPIOD
#define GPIO_Pin_CE              	GPIO_Pin_5
#define GPIO_Pin_CS              	GPIO_Pin_6
#define RCC_AHBPeriph_GPIO_CS_CE   	RCC_AHB1Periph_GPIOD

#define SPI                   		SPI1
#define GPIO_SPI              		GPIOB
#define GPIO_AF_SPI           		GPIO_AF_SPI1
#define GPIO_Pin_SPI_SCK      		GPIO_Pin_3
#define GPIO_Pin_SPI_MISO     		GPIO_Pin_4
#define GPIO_Pin_SPI_MOSI     		GPIO_Pin_5
#define RCC_APBPeriph_SPI     		RCC_APB2Periph_SPI1
//#define GPIO_Pin_SPI_CS_SOURCE          GPIO_PinSource4
#define GPIO_Pin_SPI_SCK_SOURCE         GPIO_PinSource3
#define GPIO_Pin_SPI_MISO_SOURCE        GPIO_PinSource4
#define GPIO_Pin_SPI_MOSI_SOURCE        GPIO_PinSource5
#define RCC_AHBPeriph_GPIO_SPI          RCC_AHB1Periph_GPIOB


//***************************************************//
//Define the layer1:HW operation

void nRF24L01_HW_SPI_Init(void);
void nRF24L01_HW_EXTI_Init(void);
void nRF24L01_HW_Init(void);

void nRF24L01_SPI_NSS_L(void);
void nRF24L01_SPI_NSS_H(void);
void nRF24L01_CE_L(void);
void nRF24L01_CE_H(void);
unsigned char nRF24L01_SPI_SendRecv_Byte(unsigned char dat);
void nRF24L01_SPI_Send_nByte(unsigned char *pBuf, unsigned char Len);
void nRF24L01_SPI_Recv_nByte(unsigned char *pBuf, unsigned char Len);

void nRF24L01_Delay_us(unsigned long n);